/**
 * Created by generoso on 08/02/17.
 */
public class Kalmanfilter {

    private Matrix P; //P matrix
    private Matrix X;
    private Matrix q;
    private Matrix K; //Kalman gain
    private Matrix A; //Jacobian of the prediction model
    private Matrix Q; //3x3 matrix

    //How to compute an expected range and bearing of the measurements
    private Matrix H; //Jacobian measurement model

    public Kalmanfilter(Matrix X, Matrix P, Matrix q){
        this.X = X;
        this.P = P;
        this.q = q;
        this.A = new Matrix(3, 3, 0);
        this.Q = new Matrix(3, 3, 0);
    }

    public Matrix getState(){
        return X;
    }

    public Matrix getCovariance(){
        return P;
    }

    /**
     * Aggiorna la matrice state
     * Nel paper utilizza la seguente matrice. Considerando le coordinate x, y e @
     * x + Dx
     * y + Dy
     * @ + D@
     * Ed andrà ad aggiornare le prime tre posizioni della matrice state.
     * Siccome noi riceviamo dal robottino non lo spostamento ma le coordinate effettive del robottino, possiamo
     * semplicemente sostiuire i valori dell'odometria nelle prime tre celle.
     *
     * dobbiamo aggiornare la matrice "a" per la previsione, utilizzando questa matrice
     * 1    0   -Dy
     * 0    1   Dx
     * 0    0   1
     *
     * @param xr coordinata x delle coordinate del robot
     * @param yr coordinata x delle coordinate del robot
     * @param tr rotazione rispetto all'asse x del robot
     */
    public void updateCurrentState(double xr, double yr, double tr){
        double dx = xr - X.get(0,0);
        double dy = yr - X.get(1,0);
        double dt = tr - X.get(2,0);

        X.set(0, 0, xr);
        X.set(1, 0, yr);
        X.set(1, 0, tr);

        //aggiornamento matrice A (3x3 matrice predizione)
        setPredictionModelJacobian(dx, dy);

        //aggiornamento matrice Q (3x3 matrice process noise)
        setProcessNoiseMatrix(1, dx, dy, dt);

        //Prr = A Prr A + Q
        Matrix covariance33 = P.getMatrix(0, 2, 0, 2);
        /*for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
                P.set(i, j, P.get(i, j));*/
        covariance33 = A.times(covariance33).times(A).plus(Q);

        //aggiornamento matrice covarianza
        /*for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
                P.set(i, j, covariance33.get(i, j));*/
        P.setMatrix(0, 2, 0, 2, covariance33);

        //Pri = A Pri
        int landmarkCount = (P.getColumnDimension() - 3)/2;
        System.out.println("#(landmark): " + landmarkCount);
        for(int i = 0; i < landmarkCount; i++){
            Matrix Pri = P.getMatrix(0, 2, 2 + (2*i + 1),  2 + (2*i + 2));
            Pri = A.times(Pri);
            Matrix Pir = Pri.transpose();

            P.setMatrix(0, 2, 2 + (2*i + 1),  2 + (2*i + 2), Pri);
            P.setMatrix(2 + (2*i + 1),  2 + (2*i + 2), 0, 2, Pir);
        }
    }

    public void setPredictionModelJacobian(double dx, double dy){
        A = Matrix.identity(3, 3);
        A.set(0, 2, -dy);
        A.set(1, 2, dx);
    }

    public void setProcessNoiseMatrix(double c, double dx, double dy, double dt){
        Q.set(0, 0, c * dx * dx);
        Q.set(0, 1, c * dx * dy);
        Q.set(0, 2, c * dx * dt);

        Q.set(1, 0, c * dy * dx);
        Q.set(1, 1, c * dy * dy);
        Q.set(1, 2, c * dy * dt);

        Q.set(2, 0, c * dt * dx);
        Q.set(2, 1, c * dt * dy);
        Q.set(2, 2, c * dt * dt);

    }

    /**
     * Migliorare la posizione stimata del robot aggiornando i dati dell'odometria con i dati del landmark.
     * Usando i landmark associati (parte del data association) possiamo calcolarelo spostamento (displacement) del
     * robot confrontato con DOVE PENSIAMO SI TROVI IL ROBOT. Usando il displacement possiamo aggiornare la posizione
     * del robot.
     * di cosa abbiamo bisogno?
     * Posizione del robot (x, y)
     * Posizione del landmark (lx, ly)
     *
     * Abbiamo la jacobiana H
     * xr   yr  tr  x1  y1  x2  y2  x3  y3
     * a    b   c   0   0   -a  -b  0   0
     * d    e   f   0   0   -d  -e  0   0
     *
     * con
     * a = (x-lx)/r     //
     * b = (y-ly)/r     //
     * c = 0            // valori presi dal paper
     * d = (ly-y)/r^2   // pg. 39
     * e = (lx-x)/r^2   //
     * f = -1           //
     *
     * La matrice sopra indica che su 3 landmark trovati (in totale) stiamo facendo i calcoli solo in base al
     * secondo landmark. Quindi per ogni landmark trovato nella misurazione dovremo effettuare un aggiornamento
     * consistente dei valore di H
     *
     * La matrice R del paper è la nostra matrice q, ed indica il rumore.
     * rc   @
     * @    bd
     *
     * r è il range, b è il bearing della misurazione corrente. c, d sono delle costanti che indicano la percentuale
     * di errore di misurazione.
     * Si potrebbe impostare c=0.2 per il sensore ad ultrausoni, magari aumentandolo così da considerare anche l'errore
     * della fotocamera, e d=1 per il giroscopio in quanto rivelatosi abbastanza preciso (tradotto dal paper "Un buon
     * errore per il valore bd è 1, cioè c'è un errore di un grado nella misurazione")
     *
     * @param z misurazione del landmark
     * @param r posizione del robot
     * @param associated array di landmark
     */
    public void updateStateReObservedLandmarks(double[] z, double[] r, Landmark[] associated){
        Matrix gain;
        Matrix H;

        for(Landmark l : associated){
            H = new Matrix(2, X.getColumnDimension(), 0); //Jacobiana, 2 righe, 3+n*2 colonne
            updateH(l, z, H);

            gain = calculateGain(H, P);

            Matrix Z = new Matrix(new double[][]{{z[0]}, {z[1]}});
            Matrix h = h(l, r, z);

            X = X.plus(gain.times(Z.minus(h)));
        }
    }

    /**
     * Considerendo la formula a pg. 33 del paper, supponiamo vr come la differenza nel range e vthete come la
     * differenza nel bearing
     * @param l landmark
     * @param r stato x del robot (x, y) e angolo
     * @param z misurazione effettuata
     * @return h matrice con i valori stimati (?) di range e bearing del landmark
     */
    private Matrix h(Landmark l, double[] r, double[] z){
        double vr = l.getRange() - z[0];
        double vt = l.getBearing() - z[1];

        Matrix h = new Matrix(2, 1);
        h.set(0, 0, Math.sqrt(Math.pow(l.getPos()[0] - r[0], 2) + Math.pow(l.getPos()[1] - r[1], 2)) + vr);
        h.set(1, 0, Math.atan((l.getPos()[1]-r[1])/(l.getPos()[0]-r[0]))-r[2]+vt);

        return h;
    }

    public void updateH(Landmark l, double[] z, Matrix H){
        double[][] H23= h23(l.getPos(), z, l.getRange());

        double a=H23[0][0];
        double b=H23[0][1];
        double c=0;
        double d=H23[1][0];
        double e=H23[1][1];
        double f=-1;

        //aggiornato secondo il paper pg. 39
        //aggiornamento prima riga
        H.set(0,0,a);
        H.set(0,1,b);
        H.set(0,2,c);
        H.set(0, 3+2*l.getId(), -a);
        H.set(0, 3+(2*l.getId())+1, -b);

        //aggiornamento seconda riga
        H.set(1,0,d);
        H.set(1,1,e);
        H.set(1,2,f);
        H.set(1, 3+2*l.getId(), -d);
        H.set(0, 3+(2*l.getId())+1, -e);
    }

    private double[][] h23(double[] landmarkPosition, double[] robotPosition, double r){
        double lx = landmarkPosition[0];
        double ly = landmarkPosition[1];

        double rx = robotPosition[0];
        double ry = robotPosition[1];

        double[][] result = new double[2][3];
        result[0][0] = rx-lx/r; //change in range with respect to the change in the x axis
        result[0][1] = ry-ly/r; //change in range with respect to the change in the y axis
        result[0][2] = 0; //change in range with respect to theta

        result[1][0] = ly-ry/(r*r); //this is the change in bearing for the landmark
        result[1][1] = lx-rx/(r*r); //this is the change in bearing for the landmark
        result[1][2] = -1; //this is the change in bearing for the landmark

        return result;
    }

    public Matrix calculateGain(Matrix H, Matrix P){
        Matrix S = H.times(P).times(H.transpose()).plus(q);
        return P.times(H.transpose()).times(S.inverse());
    }

    /**
     * STEP 3: add new landmarks to the current state
     *
     *
     */
    public void addNewLandmark(double xr, double yr, double br, Landmark l){
        double dx = xr - X.get(0,0);
        double dy = yr - X.get(1,0);

        //aggiornamento della matrice state
        Matrix newState = new Matrix(X.getRowDimension()+2, 1);
        for(int i = 0; i < X.getRowDimension(); i++)
            newState.set(i, 0, X.get(i, 0));
        newState.set(newState.getRowDimension()-2, 0, l.getPos()[0]);
        newState.set(newState.getRowDimension()-1, 0, l.getPos()[1]);
        X = newState;

        //aggiornamento matrice di covarianza
        Matrix newCovariance = new Matrix(P.getRowDimension()+2, P.getColumnDimension()+2);
        for(int i = 0; i < P.getRowDimension(); i++)
            for(int j = 0; j < P.getColumnDimension(); j++)
                newCovariance.set(i, j, P.get(i, j));
        P = newCovariance;

        Matrix jxr = new Matrix(new double[][]{{1, 0, -dy}, {0, 1, dx}});
        /*
        Jz=[[cos(@+d@), -t*sin(@+d@)], [sin(@+d@), t*cos(@+d@)]]
         */
        double cos = Math.cos(Math.toRadians(br));
        double sin = Math.sin(Math.toRadians(br));
        Matrix jz = new Matrix(new double[][]{{cos, -yr},{sin, xr}});
        Matrix C = jxr.times(A).times(jxr.transpose()).plus(jz.times(q).times(jz.transpose()));

        //D covarianza fra lo stato del robot e landmark (2x3)
        //E covarianza fra il landmark e lo stato del robot => E=D^t=(3x2)
        //E = PrN+1=PrrJxr^t -> (3x3) x (3x2) -> (3x2)
        //robot - landmark P for the new landmark
        Matrix E = A.times(jxr.transpose()); //3x2
        Matrix D = E.transpose(); //2x3

        //aggiunta matrice D alla matrice della covarianza
        P.setMatrix(P.getColumnDimension() - 2, P.getColumnDimension() - 1, 0, 2, D);

        //aggiunta matrice E alla matrice della covarianza
        P.setMatrix(0, 2, P.getRowDimension() - 2, P.getRowDimension() - 1, E);

        //aggiunta matrice C alla matrice di covarianza
        P.setMatrix(
                P.getRowDimension() - 2, P.getRowDimension() - 1,
                P.getColumnDimension() - 2, P.getColumnDimension() - 1,
                C
                );
        /*P.set(P.getRowDimension()-2, P.getColumnDimension()-2, C.get(0,0));
        P.set(P.getRowDimension()-2, P.getColumnDimension()-1, C.get(0,1));

        P.set(P.getRowDimension()-1, P.getColumnDimension()-2, C.get(1,0));
        P.set(P.getRowDimension()-1, P.getColumnDimension()-1, C.get(1,1));*/

        //calcolo e aggiunta delle matrici F e G alla matrice di covarianza
        try {
            for (int i = 0; i <= l.getId() - 1; i++) {
                //copiare i valori della matrice Pri
                Matrix Pri = P.getMatrix(0, 2, 2 + (2 * i + 1), 2 + (2 * i + 2));
                /*System.out.println("Pri m:" + Pri.getRowDimension() + "\tn: " + Pri.getColumnDimension());
                Matrix Prit = Pri.transpose();
                System.out.println("Prit m:" + Prit.getRowDimension() + "\tn: " + Prit.getColumnDimension());
                System.out.println("jxr m:" + jxr.getRowDimension() + "\tn: " + jxr.getColumnDimension());*/
                Matrix F = jxr.times(Pri); //sul paper usa la trasposta di Pri. Non va d'accordo con la matematica
                Matrix G = F.transpose();

                P.setMatrix(
                        P.getRowDimension()-2,
                        P.getRowDimension()-1,
                        2 + (2 * i + 1),
                        2 + (2 * i + 2), F);

                P.setMatrix(
                        2 + (2 * i + 1),
                        2 + (2 * i + 2),
                        P.getColumnDimension()-2,
                        P.getColumnDimension()-1, G);

            }
        } catch(Exception ex){
            ex.printStackTrace();
            System.exit(-1);
        }
    }
}