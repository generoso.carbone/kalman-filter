/**
 * Created by generoso on 17/02/17.
 */
public class KalmanTest {

    int landmarkCount = 0;

    public static void main(String[] args){
        Matrix state = new Matrix(3, 1, 0);
        Matrix covariance = new Matrix(3, 3, 0);
        Matrix q = new Matrix(2, 2);

        q.set(0, 0, 0.05);
        q.set(1, 1, 1);

        Kalmanfilter filter = new Kalmanfilter(state, covariance, q);

        //update odometry 1
        System.out.println("\nUpdate> x: 5\ty: 4\ttheta: 12");
        filter.updateCurrentState(5, 4, 12);
        System.out.println("State:");
        filter.getState().print(System.out);
        System.out.println("Covariance:");
        filter.getCovariance().print(System.out);

        //update odometry2
        System.out.println("\nUpdate> x: 10\ty: 8\ttheta: 12");
        filter.updateCurrentState(10, 8, 12);
        System.out.println("\nState:");
        filter.getState().print(System.out);
        System.out.println("\nCovariance:");
        filter.getCovariance().print(System.out);

        //update odometry3 + landmark 1
        System.out.println("\nUpdate> x: 10\ty: 8\ttheta: 12");
        filter.updateCurrentState(10, 8, 12);
        Landmark l = new Landmark(13, 0, new double[]{23, 8});
        filter.addNewLandmark(10, 8, 12, l);
        System.out.println("\nState:");
        filter.getState().print(System.out);
        System.out.println("\nCovariance:");
        filter.getCovariance().print(System.out);
    }
}
