/**
 * Created by generoso on 17/02/17.
 */
public class Landmark {

    private int id;
    private double range;
    private double bearing;
    private double[] pos;

    public Landmark(double range, double bearing, double[] pos){
        id++;
        this.range = range;
        this.bearing = bearing;
        this.pos = pos;
    }

    public double getRange(){
        return range;
    }

    public double getBearing(){
        return bearing;
    }

    public double[] getPos(){
        return pos;
    }

    public int getId(){
        return id;
    }

}
